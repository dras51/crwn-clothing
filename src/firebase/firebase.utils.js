import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: 'AIzaSyDhqo3RDAztWevEyH4W2MsantjHJYaSuW4',
  authDomain: 'crwn-db-66ec5.firebaseapp.com',
  databaseURL: 'https://crwn-db-66ec5.firebaseio.com',
  projectId: 'crwn-db-66ec5',
  storageBucket: 'crwn-db-66ec5.appspot.com',
  messagingSenderId: '237692912819',
  appId: '1:237692912819:web:de014c076d3ad645c69cc6',
  measurementId: 'G-PXRFM3X4XS',
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = await userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({ displayName, email, createdAt, ...additionalData });
    } catch (err) {
      console.log('error creating user', err);
    }
  }
  return userRef;
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ promt: 'select_account' });

export const signInWithGoogle = () => {
  auth.signInWithPopup(provider);
};

export default firebase;
